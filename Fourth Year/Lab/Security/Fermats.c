#include<stdio.h>
#include "../Euclidean Algo/modular_exponentiation.h"
#include<time.h>
#include<stdlib.h>

int primalityTest(int n){
    long long a = rand() % (n-4) + 2;
    long long r = modularExpo(a, n-1, n);
    printf("%lld ^ %d mod %d = %lld\n", a, n-1, n, r);
    if(r != 1) return 0;
    return 1;
}

int main(){
    printf("Enter a number: ");
    int n;
    scanf("%d", &n);
    printf("Enter Security Parameter: ");
    int t;
    scanf("%d", &t);
    srand(time(NULL));
    for(int i=0;i<t;i++){
        if(!primalityTest(n)){
            printf("Composite\n");
            return 0;
        }
    }
    printf("Prime\n");
    return 0;
}