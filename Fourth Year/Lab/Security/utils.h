#include<stdlib.h>
#include<string.h>


#define LL long long








int numBits(int n){
    int count = 0;
    while(n>0){
        count++;
        n>>=1;
    }
    return count;
}

int numDigits(int n){
    int count = 0;
    while(n){
        count++;
        n/=10;
    }
    return count;
}



char *convertIntChar(int n){
    int t = numDigits(n);
    char *ans = (char *)malloc(sizeof(char)*(t+1));
    int i=0;
    while(n>0){
        ans[t-i-1] = n%10 + '0';
        n/=10;
        i++;
    }
    ans[i] = '\0';
    return ans;
}

char *strConcat(char *a,char *b){
    int t1 = strlen(a);
    int t2 = strlen(b);
    char *ans = (char *)malloc(sizeof(char)*(t1+t2+1));
    int i=0,j=0;
    for(i=0;i<t1;i++){
        ans[i] = a[i];
    }
    for(j=0;j<t2;j++){
        ans[i+j] = b[j];
    }
    ans[i+j] = '\0';
    return ans;
}

int Kbit(int k,char *f){
    long long int end = 1<<(k-1);
    //open the file
    FILE *ptr = fopen(f,"w");
    if(ptr==NULL) return 0;
    for(long long int i=0;i<end;i++){
        fprintf(ptr,"%d\n",i+(1<<(k-1)));
    }
    fclose(ptr);
}

int markZero(int k,char *f){
    long long int end = 1<<k;
    //open the file
    FILE *ptr = fopen(f,"w");
    if(ptr==NULL) return 0;
    for(long long int i=0;i<end;i++){
        fprintf(ptr,"%d 0\n",i);
    }
    fclose(ptr);
}


typedef struct vector{
    int size;
    int capacity;
    int *arr;
}vector;

