%{
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
%}

%token DIGIT 
%left '+' '-'
%left '*' '/'

%%
s : t r { printf("\n"); };
r : '+' t  { printf("+"); } r
    |
    ;
t : DIGIT { printf("%c ", $1); }
    ;
%%

int main() {
    yyparse();
    return 0;
}

int yylex() {
    int c = getchar();
    if (isdigit(c)) {
        yylval = c;
        return DIGIT;
    }
    return c;
}

void yyerror(const char *s) {
   // fprintf(stderr, , s);
    exit(1);
}

