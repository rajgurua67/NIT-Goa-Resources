%{
    #include <stdlib.h>
    void yyerror(char *);
    #include "y.tab.h"    
%}

%%

[0-9]+ {
    yylval = atoi(yytext);
    return NUMBER;
}

[-+*/() \n] {return *yytext;}

[ \t] {;}

. {yyerror("invalid charachter");}
%%

int yywrap(void) {return 1;}

// int main(){
//     yylex();
//     return 0;
// }