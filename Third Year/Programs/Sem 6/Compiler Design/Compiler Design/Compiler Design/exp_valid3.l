%{
#include<stdlib.h>
void yyerror(char*);
#include "y.tab.h"
%}
%%
([0-9]*[.])?[0-9]+ {yylval.floating=atof(yytext);

return FLOAT;}
[-+*/()\n] {
    return *yytext;
}
[\t] { }
. {yyerror("Invalid Character");}
%%
int yywrap(void){return 1;}