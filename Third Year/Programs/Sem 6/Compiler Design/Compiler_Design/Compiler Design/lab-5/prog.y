%{
    #include<stdio.h>
    #include<stdlib.h>
    void yyerror(char*s);
    extern yyval;
%}

%define api.value.type union
%token ID NUM IF ELSE print
%right '=' '<' '>'
%left '+' '-'

%%
pgm : stList { printf("Congrats your input is accepted"); return 0;}
stList : stList stmt
    | stmt
    ;

stmt : ID '=' expr ';' { $$ = $3; $1->value.var = $3; printf("value of c is %d",$3); }
    | IF '(' TERM '>' NUM ')' '{' stList '}' ELSE '{' stList '}'
    | print ID { printf("%d",$2);} 
    ;

expr : expr '+' TERM { $$ = $1 + $3;}
    | expr '-' TERM { $$ = $1 - $3;}
    | TERM { $$ = $1; }
    ;

TERM : ID { $$ = $1;}
    | NUM { $$ = $1;}
    ;

%%

void yyerror(char *msg){
    printf("Invalid expression");
    exit(1);
}
int main(){
    yyparse();
    printf("Hello I am good");
    
    return 0;
}






