%{
    #include <stdlib.h>
    void yyerror(char *);
    #include "y.tab.h"    
%}

%%
"if" return IF;
"else" return ELSE;
[a-zA-Z] {
    yylval = *yytext - 'a';
    return IDENTIFIER;
}

[0-9]+ {
    yylval = atoi(yytext);
    return NUMBER;
}

[-+*/();{}=] {return *yytext;}

[ \t\n] {;}

. {yyerror("invalid charachter");}
%%

int yywrap(void) {return 1;}